package infra.api.entity

import infra.api.util.math.InfraBoundingBox
import infra.processors.injection.Inject
import infra.processors.injection.InjectionType.INTRINSIC
import infra.processors.injection.InjectionType.METHOD
import infra.processors.injection.TargetType

@TargetType("net.minecraft.entity.Entity")
interface InfraEntity {
    var isSprinting: Boolean

    @Inject("", "getBoundingBox", versions = ["^1.8.9", "^1.14.4"], types = [INTRINSIC, METHOD])
    fun getEntityBoundingBox(): InfraBoundingBox
}