package infra.api.entity.player

import infra.processors.injection.TargetType

@TargetType(
    "net.minecraft.entity.player.EntityPlayer",
    "net.minecraft.entity.player.PlayerEntity",
    versions = ["1.8.9", "^1.14.4"]
)
interface InfraPlayer {
}