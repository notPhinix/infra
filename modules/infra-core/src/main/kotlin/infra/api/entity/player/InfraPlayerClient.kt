package infra.api.entity.player

import infra.api.entity.InfraEntity
import infra.processors.injection.TargetType

@TargetType(
    "net.minecraft.client.entity.EntityPlayerSP",
    "net.minecraft.client.network.ClientPlayerEntity",
    versions = ["1.8.9", "^1.14.4"]
)
interface InfraPlayerClient : InfraEntity {

//    /**
//     * The last sneak flag that was sent to the server
//     */
//    val serverSneakState: Boolean
//        @Inject("serverSneakState") get
//
//    /**
//     * The last sprint flag that was sent to the server
//     */
//    val serverSprintState: Boolean
//        @Inject("serverSprintState") get
}