package infra.api.render.tessellator

import infra.processors.injection.Inject
import infra.processors.injection.InjectionType.INTRINSIC
import infra.processors.injection.InjectionType.METHOD
import infra.processors.injection.TargetType

@TargetType(
    "net.minecraft.client.renderer.Tessellator",
    "net.minecraft.client.render.Tessellator",
    versions = ["^1.8.9", "^1.14.4"]
)
interface InfraTessellator {

    val vertexBufferBuilder: InfraVertexBufferBuilder
        @Inject("getWorldRenderer", "getBufferBuilder", versions = ["^1.8.9", "^1.14.4"], types = [METHOD, METHOD]) get

    @Inject("draw", types = [INTRINSIC])
    fun draw()
}