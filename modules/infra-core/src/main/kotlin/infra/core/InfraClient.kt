package infra.core

import infra.api.client.InfraMinecraft
import infra.core.command.CommandInterpreter
import infra.core.gui.DisplayServer
import infra.core.modulesystem.ModuleManager
import infra.core.system.LoaderSubsystem
import infra.core.system.SubSystem
import org.apache.logging.log4j.LogManager

object InfraClient {

    const val CLIENT_BRAND = "Infra"
    const val CLIENT_SEM_VERSION = "0.0.1-alpha1"

    val logger = LogManager.getLogger()!!

    lateinit var minecraft: InfraMinecraft
        private set

    /**
     * A list of all subsystems that make up the client
     */
    private val subsystems = listOf(
        ModuleManager,
        CommandInterpreter,
        DisplayServer,
        LoaderSubsystem
    )

    fun init(infraMinecraft: InfraMinecraft) {
        logger.info("$CLIENT_BRAND [$CLIENT_SEM_VERSION] client initialization...")
        minecraft = infraMinecraft

        // initialize every subsystem sorted by their natural list order
        subsystems.forEach(SubSystem<*>::init)

        logger.info("detected display dimensions ${infraMinecraft.displayWidth}x${infraMinecraft.displayHeight}")
        logger.info("client initialization complete")
    }
}