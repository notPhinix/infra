package infra.core.bus.render

import infra.core.bus.Event

class RenderOverlayEvent(val partialTicks: Float) : Event