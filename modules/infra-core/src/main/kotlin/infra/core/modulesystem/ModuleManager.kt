package infra.core.modulesystem

import infra.core.system.ModuleLoader
import infra.core.system.SubSystem

/**
 * Central management of modules and module loading
 */
object ModuleManager : SubSystem<Module> {

    private val modulesList = mutableListOf<Module>()

    /**
     * Initialize the [ModuleManager] and register all [Module]s of a [ModuleLoader]
     */
    override fun init() {

    }

    /**
     * Register and run the init() Method for a single [Module]
     */
    override fun registerModule(module: Module) {
        this.modulesList += module.also {
            it.init()
        }
    }

    /**
     * Register and run the init() Method for a list of [Module]s
     */
    fun registerModules(modules: List<Module>) {
        this.modulesList += modules.also { module ->
            module.forEach(Module::init)
        }
    }

    override fun getModules(): List<Module> {
        return this.modulesList
    }

    /**
     * @return a list of [Module]s sorted by the length of the display name
     */
    fun getModulesByLength(): List<Module> {
        return getModulesByAlphabet()
            .sortedWith(Comparator { moduleA, moduleB -> moduleB.displayName.length - moduleA.displayName.length })
    }

    /**
     * @return a list of [Module]s sorted by the alphabet of the display name
     */
    fun getModulesByAlphabet(): List<Module> {
        return modulesList.sortedBy { it.displayName }
    }
}