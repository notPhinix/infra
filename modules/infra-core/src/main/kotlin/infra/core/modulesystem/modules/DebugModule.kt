package infra.core.modulesystem.modules

import infra.core.InfraClient
import infra.core.bus.client.MinecraftTickEvent
import infra.core.bus.render.RenderOverlayEvent
import infra.core.bus.render.RenderWorldEvent
import infra.core.modulesystem.Module
import net.techcable.event4j.EventHandler

class DebugModule : Module("DebugModule", "debug", hide = true) {

    override fun onInitialize() {
        InfraClient.logger.info("debug module initialized")
    }

    @EventHandler
    fun onTick(e: MinecraftTickEvent) {

    }

    @EventHandler
    fun onRenderWorld(e: RenderWorldEvent) {

    }

    @EventHandler
    fun onRenderOverlay(e: RenderOverlayEvent) {

    }
}