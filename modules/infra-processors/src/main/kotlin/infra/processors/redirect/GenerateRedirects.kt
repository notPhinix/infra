package infra.processors.redirect

import kotlin.reflect.KClass

/**
 * Annotation to trigger generation of redirect macros in version modules
 *
 * @param values classes to generate redirects for
 * @param mixinMetaFileName file name where to list redirect mixins
 */
annotation class GenerateRedirects(
    vararg val values: KClass<out Any>,
    val mixinMetaFileName: String = "autoredirects.infra.json"
)