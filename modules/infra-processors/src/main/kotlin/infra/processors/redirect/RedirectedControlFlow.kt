package infra.processors.redirect

/**
 * A macro that marks control flow as never returning. The code of this macro should never be actually called, it
 * should be replaced by a mixin-injected call to another function.
 */
@Suppress("FunctionName", "NOTHING_TO_INLINE")
inline fun RedirectedControlFlow(): Nothing {
    throw AssertionError("this control flow should have been redirected by a mixin")
}