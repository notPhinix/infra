package infra.mixin.hooks;

import infra.core.bus.EventBroker;
import infra.core.bus.render.RenderOverlayEvent;
import net.minecraft.client.gui.GuiIngame;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(GuiIngame.class)
public class RenderOverlayHook {

    @Inject(method = "renderGameOverlay", at = @At("RETURN"))
    protected void onRenderGameOverlay(float partialTicks, CallbackInfo info) {
        EventBroker.INSTANCE.dispatchEvent(new RenderOverlayEvent(partialTicks));
    }
}
